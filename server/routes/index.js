const express = require('express');
const router = express.Router();
const customer = require('../controllers/customer_upc_controller');

// Customer UPC Curd RESTful Api
router.post('/customer/create',customer.create_customer);
router.put('/customer/update/:id',customer.update_customer);
router.get('/customer/single/view/:id',customer.single_view_customer); 
router.get('/customer/list/:page',customer.customer_list);
router.get('/customer/delete/:id',customer.delete_customer);



module.exports =router;