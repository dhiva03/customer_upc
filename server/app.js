const express = require('express'); // Fast, unopinionated, minimalist web framework for node.
const path = require('path'); // NodeJS Package for file paths bodyParser  from 'body-parser'; // Parse incoming request bodies in a middleware before your handlers, available under the req.body property.
const cors = require('cors'); // CORS is a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options.
const bodyParser  = require('body-parser'); // Parse incoming request bodies in a middleware before your handlers, available under the req.body property.
const logger = require('./logger').logger;
const morgan = require('morgan');// Import morgan
const router = express.Router(); // Creates a new router object.
const port = process.env.PORT || 3000; // Allows heroku to set port
const app = express(); // Initiate Express Application
const routes = require('./routes/index');


// Middleware
app.use(morgan('dev')); // Morgan Middleware
app.use(cors({origin: '*'})); // Allows cross origin in development only
app.use(bodyParser.urlencoded({extended: false})); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(express.static(__dirname + '/public')); // Provide static directory for frontend

// use the Api routes
app.use('/rest/api',routes);

// load the UI form Public 
app.get('*', (req, res) =>{
    res.sendFile(path.join(__dirname + '/public/index.html'));   
});

// Start Server: Listen on port 3000
app.listen(port,()=> {
    console.log('Listening on port ' + port);
});