'use strict';
module.exports = (sequelize, DataTypes) => {
    var Customer = sequelize.define('Customers_UPC', {
        BasketID: {type: DataTypes.STRING, allowNull: false},
        CustomerName: {type: DataTypes.STRING, allowNull: false},
        UPC1: {type: DataTypes.BIGINT, allowNull: false},
        UPC2: {type: DataTypes.BIGINT, allowNull: false},
        UPC3: {type: DataTypes.BIGINT, allowNull: false},
        UPC4: {type: DataTypes.BIGINT, allowNull: false},
        UPC5: {type: DataTypes.BIGINT, allowNull: false},
        UPC6: {type: DataTypes.BIGINT, allowNull: false},
    }, {timestamps: true, tableName: 'Customers_UPC'});
    Customer.associate = function (models) {
        // associations can be defined here
    };
    return Customer;
};