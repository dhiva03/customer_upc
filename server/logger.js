const winston = require('winston');

/* initialize logger */
exports.logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        //
        // - Write to all logs with level `info` and below to `logger.log`
        //
        new winston.transports.File({ filename: './logs/logger.log' })
    ]
});
