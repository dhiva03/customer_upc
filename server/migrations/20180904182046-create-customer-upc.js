'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Customers_UPC', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            BasketID: {
                type: Sequelize.STRING(256),
                allowNull: false,
                unique: true
            },
            CustomerName: {
                type: Sequelize.STRING(256),
                validate: {
                    is: {
                        args: ["^[a-z]+$", 'i'],
                        msg: "Must be a character only"
                    }
                }
            },
            UPC1: {
                type: Sequelize.BIGINT(255),
                allowNull: false,
                unique: true
            },
            UPC2: {
                type: Sequelize.BIGINT(255),
                allowNull: false,
                unique: true
            },
            UPC3: {
                type: Sequelize.BIGINT(255),
                allowNull: false,
                unique: true
            },
            UPC4: {
                type: Sequelize.BIGINT(255),
                allowNull: false,
                unique: true
            },
            UPC5: {
                type: Sequelize.BIGINT(255),
                allowNull: false,
                unique: true
            },
            UPC6: {
                type: Sequelize.BIGINT(255),
                allowNull: false,
                unique: true
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Customers_UPC');
    }
}; 