// import db models
const db = require('../models/index');

// function to create customer 
exports.create_customer = (req, res) => {
    var requestData = Object.setPrototypeOf(req.body, null);
    db.Customer.create(requestData).then((success) => {
        if (success) {
            res.status(200).json({"status": "success", "message": "customer created successfully"});
        }
    }).catch((err) =>{
        res.status(500).send('Internal Server Error');
    });
}

// function to update customer
exports.update_customer =  (req, res)=> {
    if (req.params.id != 'undefined' && req.params.id != null) {
        db.Customer.update(req.body, {where: {id: req.params.id}}).then(function (success) {
            if (success[0] != 0) {
                res.status(200).json({"status": "success", "message": "customer updated successfully"});
            } else {
                res.status(404).json({"status": "failure", "message": "customer  not found for the given id"});
            }
        }).catch((err) =>{
            res.status(500).send('Internal Server Error');
        });
    } else {
        res.status(400).send('Bad Request');
    }
}

// function to view single customer
exports.single_view_customer = (req, res) =>{
    if (req.params.id != 'undefined' && req.params.id != null) {
        db.Customer.findById(req.params.id).then(function (success) {
            if (success) {
                res.status(200).json({"status": "success", "result": success});
            } else {
                res.status(404).json({"status": "failure", "message": "Customer not found for the given id"});
            }
        }).catch((err) =>{
            res.status(500).send('Internal Server Error');
        });
    } else {
        res.status(400).send('Bad Request');
    }
}

// function to delete a customer
exports.delete_customer =(req, res) =>{
    if (req.params.id != 'undefined' && req.params.id != null) {
        db.Customer.destroy({
            where: {
                'id': req.params.id
            }
        }).then((success) =>{
            if (success) {
                res.status(200).json({"status": "success", "message": "Customer delete successfully"});
            } else {
                res.status(404).json({"status": "failure", "message": "Customer not found for the given id"});
            }
        }).catch((err) =>{
            res.status(500).send('Internal Server Error');
        });
    } else {
        res.status(400).send('Bad Request');
    }
}

// function to view list of customer
exports.customer_list = (req, res) => {
    let limit = 2;
    let offset = 0;
    db.Customer.findAndCountAll()
        .then((data) => {
            let page = req.params.page;      // page number
            let pages = Math.ceil(data.count / limit);
            offset = limit * (page - 1);
            db.Customer.findAll({
                limit: limit,
                offset: offset,
                $sort: {id: 1}
            }).then((customer) => {
                res.status(200).json({
                    "status": "success",
                    'result': {'customerList': customer, 'count': data.count, 'page': req.params.page, 'limit': limit}
                });
            });
        }).catch((error)=> {
        res.status(404).json({"status": "failure", "message": "No Data Found"});
    });
}
